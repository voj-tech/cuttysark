set background=light

highlight clear
if exists('syntax_on')
  syntax reset
endif

let g:colors_name = 'cuttysark-ergo'

highlight Normal           guifg=#222222 guibg=#e8e8e8

highlight Comment          guifg=#777777
highlight SpecialComment   guifg=#777777                  gui=bold        cterm=bold
highlight String           guifg=#821f01
highlight Character        guifg=#821f01
highlight Constant         guifg=#820128
highlight Boolean          guifg=#961574                  gui=bold        cterm=bold
highlight Operator         guifg=#111111
highlight Statement        guifg=#961574                  gui=bold        cterm=bold
highlight StorageClass     guifg=#961574                  gui=bold        cterm=bold
highlight Keyword          guifg=#961574                  gui=bold        cterm=bold
highlight Type             guifg=#961574                  gui=bold        cterm=bold
highlight Function         guifg=#08225c                  gui=bold        cterm=bold
highlight Identifier       guifg=#213b75                  gui=bold        cterm=bold
highlight PreProc          guifg=#770000
highlight Underlined       guifg=#0000ff                  gui=underline
highlight Todo             guifg=#0073bf guibg=NONE       gui=bold,italic cterm=bold,italic

highlight Special          guifg=#820128

highlight Visual                         guibg=#cccccc
highlight VisualNOS                                       gui=reverse     cterm=reverse

highlight Folded           guifg=#111111 guibg=#cccccc    gui=NONE
highlight Conceal          guifg=#111111 guibg=#cccccc    gui=NONE

highlight StatusLineNC     guifg=#000000 guibg=#d3d3d3    gui=NONE        cterm=NONE
highlight VertSplit        guifg=#d3d3d3 guibg=#d3d3d3
highlight StatusLine       guifg=#000000 guibg=#ffffff    gui=NONE        cterm=NONE
highlight StatusLineTerm                 guibg=FireBrick3
highlight StatusLineTermNC               guibg=FireBrick4

highlight LineNr           guifg=#777777
highlight CursorLine                     guibg=#f0f0f0                    cterm=NONE
highlight CursorLineNr     guifg=#777777 guibg=#f0f0f0    gui=NONE        cterm=NONE
highlight CursorColumn                   guibg=#f0f0f0
highlight SignColumn       guifg=#777777 guibg=NONE

highlight SpellBad                       guibg=#ffd7d7    gui=NONE
highlight SpellCap                       guibg=#5fd7ff    gui=NONE
highlight SpellRare                      guibg=#ffd7ff    gui=NONE
highlight SpellLocal                     guibg=#54ffff    gui=NONE

highlight ColorColumn                    guibg=#e0e0e0
highlight DiffAdd                        guibg=#bee6be
highlight DiffChange                     guibg=#cad9fa
highlight DiffDelete       guifg=#000000 guibg=#d6d6d6    gui=NONE
highlight DiffText                       guibg=#cad9fa    gui=bold
highlight Directory        guifg=#213b75
highlight ErrorMsg         guifg=#961574 guibg=NONE       gui=bold        cterm=bold
highlight Error            guifg=#ff0000 guibg=NONE
highlight FoldColumn       guifg=#f0f0f0 guibg=NONE
highlight IncSearch                      guibg=#bbbbbb    gui=NONE        cterm=NONE
highlight MatchParen                     guibg=#99ccff
highlight ModeMsg                                         gui=bold
highlight MoreMsg          guifg=#820128                  gui=bold        cterm=bold
highlight NonText          guifg=#808080                  gui=NONE
highlight Pmenu                          guibg=#ebf4fe
highlight PmenuSel                       guibg=#a0b7c7
highlight PmenuSbar                      guibg=#f2f2f2
highlight PmenuThumb                     guibg=#d9d9d9
highlight Question         guifg=#820128                  gui=bold        cterm=bold
highlight Search                         guibg=#dddddd
highlight SpecialKey       guifg=#c5c5c5
highlight TabLine                        guibg=#d3d3d3    gui=NONE        cterm=NONE
highlight TabLineSel                     guibg=#ffffff    gui=NONE        cterm=NONE
highlight TabLineFill                    guibg=#f2f2f2    gui=NONE        cterm=NONE
highlight Title            guifg=#961574                  gui=bold        cterm=bold
highlight WarningMsg       guifg=#213b75                  gui=bold        cterm=bold
highlight WildMenu                       guibg=#f6ebbc

highlight GitGutterAdd     guifg=#c9dec1 guibg=#c9dec1
highlight GitGutterChange  guifg=#c3d6e8 guibg=#c3d6e8
highlight GitGutterDelete  guifg=#9f9f9f

highlight! link diffAdded   DiffAdd
highlight! link diffRemoved DiffDelete

" NERDTree

highlight! link NERDTreeHelp Comment

" Tagbar

highlight TagbarAccessPublic    guifg=#808080
highlight TagbarAccessProtected guifg=#808080
highlight TagbarAccessPrivate   guifg=#961574

highlight! link TagbarScope    Directory
highlight! link TagbarFoldIcon Directory
