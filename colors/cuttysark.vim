set background=light

highlight clear
if exists('syntax_on')
  syntax reset
endif

let g:colors_name = 'cuttysark'

highlight Normal                guifg=Black         guibg=White

highlight Comment               guifg=Green4
highlight String                guifg=Brown
highlight Character             guifg=Brown
highlight Constant              guifg=LightSeaGreen
highlight Operator              guifg=Black
highlight Statement             guifg=MediumBlue                     gui=bold
highlight StorageClass          guifg=MediumBlue                     gui=bold
highlight Keyword               guifg=MediumBlue                     gui=bold
highlight Type                  guifg=Black                          gui=NONE
highlight Function              guifg=Black
highlight Identifier            guifg=Black
highlight PreProc               guifg=DarkGoldenrod
highlight Underlined            guifg=MediumBlue                     gui=underline
highlight Todo                  guifg=Green4        guibg=Yellow

highlight Special               guifg=Brown

highlight Visual                                    guibg=#86abd9
highlight VisualNOS                                                  gui=reverse

highlight Folded                guifg=#959595       guibg=#ececec    gui=NONE
highlight Conceal               guifg=#959595       guibg=#ececec    gui=NONE

highlight StatusLineNC          guifg=Black         guibg=Gray75     gui=NONE
highlight VertSplit             guifg=Gray75        guibg=Gray75
highlight StatusLine            guifg=Black         guibg=Gray85     gui=bold
highlight StatusLineTerm                            guibg=FireBrick3
highlight StatusLineTermNC                          guibg=FireBrick4

highlight LineNr                guifg=#959595       guibg=#ececec
highlight CursorLine                                guibg=#fff4ce
highlight CursorLineNr          guifg=Brown         guibg=#ececec
highlight CursorColumn                              guibg=#ececec
highlight SignColumn                                guibg=#ececec

highlight DiffAdd                                   guibg=LightGreen
highlight DiffChange                                guibg=LightBlue
highlight DiffDelete            guifg=Black         guibg=LightRed   gui=NONE
highlight DiffText                                  guibg=LightBlue  gui=bold
highlight Directory             guifg=MediumBlue
highlight ErrorMsg              guifg=Red           guibg=NONE
highlight Error                 guifg=Red           guibg=NONE
highlight FoldColumn            guifg=MediumBlue    guibg=Gray
highlight IncSearch                                                  gui=reverse
highlight ModeMsg                                                    gui=bold
highlight MoreMsg               guifg=SeaGreen                       gui=bold
highlight NonText               guifg=Gray                           gui=bold
highlight Pmenu                                     guibg=Gray85
highlight PmenuSel              guifg=White         guibg=#86abd9
highlight PmenuSbar                                 guibg=Gray85
highlight PmenuThumb                                guibg=Gray75
highlight Question              guifg=SeaGreen                       gui=bold
highlight Search                                    guibg=Yellow
highlight SpecialKey            guifg=MediumBlue
highlight TabLine                                   guibg=Gray75     gui=NONE
highlight TabLineSel                                guibg=Gray85
highlight TabLineFill                               guibg=Gray75
highlight Title                 guifg=MediumBlue                     gui=bold
highlight WarningMsg            guifg=Red4
highlight WildMenu                                  guibg=Yellow

highlight GitGutterAdd          guifg=Green4        guibg=#ececec
highlight GitGutterChange       guifg=MediumBlue    guibg=#ececec
highlight GitGutterDelete       guifg=Red           guibg=#ececec
highlight GitGutterChangeDelete guifg=MediumBlue    guibg=#ececec
