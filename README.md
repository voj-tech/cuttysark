cuttysark
=========

Synopsis
--------

cuttysark is a collection of two Vim colour schemes&mdash;cuttysark and cuttysark-ergo.

cuttysark
---------

Loosely based on the BlueSmoke colour scheme which is a light scheme with syntax highlighting resembling
colours of Visual Studio 6.0 and early .NET. The BlueSmoke colour scheme used to be available at
[https://github.com/dfrunza/vim/tree/master/colors](https://github.com/dfrunza/vim/tree/master/colors). That
does not seem to be the case anymore however you might be able to find it bundled with other colour schemes
in a GitHub repository at [https://github.com/gmist/vim-palette](https://github.com/gmist/vim-palette).

Note that the cuttysark colour scheme has not really been properly debugged and there are likely to be
inconsistencies.

cuttysark-ergo
--------------

A port of the [Ergo](http://color-themes.com/?view=theme&id=563a1a6d80b4acf11273ae72) IntelliJ IDEA colour
theme by “Stefan”.

Optimised for use with True Color enabled terminals with `termguicolors` set.

Authors
-------

Vojtěch Sobota ([www.voj-tech.net](http://www.voj-tech.net/)).

License
-------

CC BY-SA 4.0 license. See the `LICENSE` file for details.
